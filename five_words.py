import time

start = time.time()
count = 0

wordsfile = r"words_alpha.txt"

# create the candidate word dictionary
# collect all words of length 5 with 5 distinct letters
with open(wordsfile, 'r') as f:
    words = list(filter(lambda w:len(w) == 5 and len(set(w)) == 5, map(str.strip, f.readlines())))

print('words done', (time.time() - start)/60.0)

# for each word, find the set of disjoint words that alphabetically follow it
disjoint_words = {}
while words:
    w = words.pop()
    ws = set(w)
    disjoint_words[w] = {dw for dw in words if ws.isdisjoint(set(dw))}

print('disjoint words done', (time.time() - start)/60.0)

# a generator to yield word lists given a set of candidate words and the list of words found so far
def find_five_words(word_pool, so_far):
    global count
  
    # if we already have four words, then all the remaining disjoint words in the pool are good
    if so_far >= 4:
        for w in word_pool:
            yield w,
        return

    # otherwise search the rest of the word pool for possible continuations
    for w in word_pool:

        # log progress
        if so_far == 0:
            count += 1
            if count % 100 == 0:
                print(count, (time.time() - start)/60.0)

        for result in find_five_words(word_pool.intersection(disjoint_words[w]), so_far + 1):
            yield w,*result

set_of_fives = set()
for five_words in find_five_words(set(disjoint_words), 0):
    set_of_fives.add(tuple(sorted(five_words)))
    print('**********', five_words)

print((time.time() - start)/60.0)
